'''
Define the Flask application instance by 
importing the app variable which is a member of the app package.
'''
from app import app, db
from app.models import User, Post

@app.shell_context_processor
def make_shell_context():
    '''
    Create a shell context that 
    adds the database instance and models to the shell session.
    '''
    return {'db': db, 'User': User, 'Post': Post}