'''Simple email framework for Microblog app'''
from threading import Thread
from flask import render_template
from flask_mail import Message
from app import app
from app import mail

def send_async_email(app, msg):
    '''Implement the sending of emails in the background'''
    with app.app_context():
        mail.send(msg)

def send_email(subject, sender, recipients, text_body, html_body):
    '''Helper function to enable sending of email'''
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    # Invoke the running of sending emails in the background
    Thread(target=send_async_email, args=(app, msg)).start()

def send_password_reset_email(user):
    '''Generate password reset emails'''
    token = user.get_reset_password_token()
    send_email(
        '[Microblog] Reset Your Password', 
        sender=app.config['ADMINS'][0], 
        recipients=[user.email], 
        text_body=render_template('email/reset_password.txt', user=user, token=token), 
        html_body=render_template('email/reset_password.html', user=user, token=token)
    )