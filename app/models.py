'''Entity class for database objects'''
import jwt
from time import time
from datetime import datetime
from hashlib import md5
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import app
from app import db
from app import login

# Add the followers to the database
followers = db.Table(
    'followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
    )

class User(UserMixin, db.Model):
    '''Entity representing the user fields and methods for the User table'''
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    # Declare a one-to-many relationship (user->posts)
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    # Declare many-to-many relationship in the users table
    followed = db.relationship(
        'User',
        secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'), 
        lazy='dynamic'
        )

    def set_password(self, password):
        '''Set a user's password by hashing it'''
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        '''Check if the generated hash matches the user's password'''
        return check_password_hash(self.password_hash, password)

    def get_reset_password_token(self, expires_in=600):
        '''Generate a JWT token'''
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in}, 
            app.config['SECRET_KEY'], 
            algorithm='HS256'
        ).decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        '''Verify if the token is valid / expired'''
        try:
            id = jwt.decode(
                token, 
                app.config['SECRET_KEY'], 
                algorithms=['HS256']
            )['reset_password']
        except:
            return
        return User.query.get(id)

    def avatar(self, size):
        '''Generate avatar urls for the user'''
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size
        )

    def follow(self, user):
        '''Follow a user'''
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        '''Unfollow a user'''
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        '''Return the number of the following count'''
        return self.followed.filter(followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):
        '''Fetch all the posts of the followed users'''
        followed = Post.query.join(
            followers, 
            (followers.c.followed_id == Post.user_id)
            ).filter(
                followers.c.follower_id == self.id
            )
        own = Post.query.filter_by(user_id=self.id)
        return followed.union(own).order_by(Post.timestamp.desc())

    def __repr__(self):
        return '<User {}>'.format(self.username)

class Post(db.Model):
    '''Entity representing the post fields and methods for the Post table'''
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Post {}>'.format(self.body)

@login.user_loader
def load_user(id):
    '''Load a user given an ID'''
    return User.query.get(int(id))
