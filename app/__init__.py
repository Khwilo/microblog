'''Flask application instance'''
import logging
import os
from logging.handlers import SMTPHandler, RotatingFileHandler
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_mail import Mail
from flask_bootstrap import Bootstrap
from flask_moment import Moment

# Create the app object as an instance of the class Flask
app = Flask(__name__)
# Read and apply the configuration settings from the config file
app.config.from_object(Config)
# Create the database application instance
db = SQLAlchemy(app)
# Create an object representing the Migration engine
migrate = Migrate(app, db)
# Create and initialize the LoginManager
login = LoginManager(app)
# Register the view function that handles loggin in
login.login_view = 'login'
# Create an instance of the Flask Mail extension
mail = Mail(app)
# Initialize Twitter Bootstrap
bootstrap = Bootstrap(app)
# Initialize Flask Moment
moment = Moment(app)

'''
The app package is defined by the app directory and this script 
and referenced here.
'''
from app import routes, models, errors

if not app.debug:
    # Send logs to email
    if app.config['MAIL_SERVER']:
        auth = None
        if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
            auth = (app.config['MAIL_USERNAME'], app.config['MAIL_PASSWORD'])
        secure = None
        if app.config['MAIL_USE_TLS']:
            secure = ()
        mail_handler = SMTPHandler(
            mailhost=(app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
            fromaddr='no-reply@' + app.config['MAIL_SERVER'],
            toaddrs=app.config['ADMINS'], 
            subject='Microblog Failure',
            credentials=auth,
            secure=secure
        )
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

    # Send logs to file
    if not os.path.exists('logs'):
        os.mkdir('logs')
    file_handler = RotatingFileHandler(
        'logs/microblog.log', maxBytes=10240, backupCount=10
        )
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'
    ))
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)

    app.logger.setLevel(logging.INFO)
    app.logger.info('Microblog startup')
